@extends('layouts.layout')
@section('title','Listar')
@section('content')
<div class="row">
	<section class="content">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="pull-left"><h3>Lista Libros</h3></div>
					<div class="pull-right">
						<div class="btn-group">
							<a href="{{ url('/create') }}" class="btn btn-info" >Añadir Libro</a>
						</div>
					</div>
					<div class="table-container">
						<table id="mytable" class="table table-bordred table-striped">
							<thead>
								<th>Nombre</th>
								<th>Resumen</th>
								<th>No. Páginas</th>
								<th>Edicion</th>
								<th>Autor</th>
								<th>Precio</th>
								<th>Editar</th>
								<th>Eliminar</th>
							</thead>
							<tbody>
								@if($libros->count())  
								@foreach($libros as $libro)  
								<tr>
									<td>{{$libro->nombre}}</td>
									<td>{{$libro->resumen}}</td>
									<td>{{$libro->npagina}}</td>
									<td>{{$libro->edicion}}</td>
									<td>{{$libro->autor}}</td>
									<td>{{$libro->precio}}</td>
									<td>
										<a class="btn btn-primary btn-xs" href=""><span class="glyphicon glyphicon-pencil"></span></a>
									</td>
									<td>
										<form action="{{route('Del')}}" method="get">{{--csrf_field()--}}
											<input name="id" value="{{$libro->id}}" type="hidden" value="DELETE">

											<button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach 
                                @else
                                <tr>
                                  <td colspan="8">No hay registro !!</td>
                              </tr>
                              @endif
                          </tbody>
                          <tfoot>
                            <th>Nombre</th>
                            <th>Resumen</th>
                            <th>No. Páginas</th>
                            <th>Edicion</th>
                            <th>Autor</th>
                            <th>Precio</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="container">
                {{ $libros->links() }}
            </div>
        </div>
    </div>
</section>
@endsection