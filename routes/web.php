<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|


Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/','LibroController@index')->name('home');

Route::get('/create','LibroController@create');

Route::get('Nuevo','LibroController@store')->name('Nuevo');

Route::get('/Del','LibroController@destroy')->name('Del');