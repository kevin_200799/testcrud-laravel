<?php

namespace App\Http\Controllers;
use App\Libro;
use Illuminate\Http\Request;

class LibroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $libros = Libro::orderBy('id','DESC')->paginate(3);
        return view('Libro.index',compact('libros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Libro.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     
    public function store(Request $request)
    {
        //
        $this->validate($request,['nombre'=>'required','resumen'=>'required','npagina'=>'required','edicion'=>'required','autor'=>'required','precio'=>'required']);
        Libro::create($request->all());
        return redirect()->route('Libro.index')->with('success','Agregado con exito');
    }*/
    public function store(Request $request)
    {
       $hole = new Libro;
       $hole->nombre = $request->nombre;
       $hole->resumen = $request->resumen;
       $hole->npagina = $request->npagina;
       $hole->edicion = $request->edicion;
       $hole->autor = $request->autor;
       $hole->precio = $request->precio;

       $hole->save();
       return redirect()->route('home')->with('success','Eliminado con exito');
   }

    /**
     * Display the specified resource.
     *
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function show(Libro $id)
    {
        //
        $libros = Libro::find($id);
        return view('Libro.show',compact('libros'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function edit(Libro $id)
    {
        //
        $libro = Libro::find($id);
        return view('Libro.edit',compact('libros'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Libro $id)
    {
        //
       $this->validate($request,['nombre'=>'required','resumen'=>'required','npagina'=>'required','edicion'=>'required','autor'=>'required','precio'=>'required']);
       libro::find($id)->update($request->all());
       return redirect()->route('Libro.index')->with('success','Actualizado con exito');
   }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Libro  $libro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Libro $id)
    {
        //
        Libro::find($id)->delete();
        return redirect()->route('Home')->with('success','Eliminado con exito');
    }
}
