<?php

namespace App\Http\Controllers;

use App\Catastro;
use Illuminate\Http\Request;

class CatastroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Catastro  $catastro
     * @return \Illuminate\Http\Response
     */
    public function show(Catastro $catastro)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Catastro  $catastro
     * @return \Illuminate\Http\Response
     */
    public function edit(Catastro $catastro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Catastro  $catastro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Catastro $catastro)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Catastro  $catastro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Catastro $catastro)
    {
        //
    }
}
